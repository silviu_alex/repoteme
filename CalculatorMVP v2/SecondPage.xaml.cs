﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
namespace Calculator_MVP
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    public partial class Page1 : Window
    {
        public Page1()
        {
            InitializeComponent();
        }
        private bool waitForClick = true;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (waitForClick == true)
            {
                int coreCount = 0;
                ProcessorsDisplay.Text += "Numarul de procesoare fizice din sistem este ";
                foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_ComputerSystem").Get())
                {
                    ProcessorsDisplay.Text += (item["NumberOfProcessors"]).ToString();
                    ProcessorsDisplay.Text += "\n";
                }
                ProcessorsDisplay.Text += "Numarul de nuclee din sistem este ";
                foreach (var item in new System.Management.ManagementObjectSearcher("Select * from Win32_Processor").Get())
                {
                    coreCount += int.Parse(item["NumberOfCores"].ToString());
                }
                ProcessorsDisplay.Text += coreCount;
                waitForClick = false;
            }
        }
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start("https://intranet.unitbv.ro/Acasa");
        }
    }
}
