﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace Calculator_MVP
{
    public partial class MainWindow : Window
    {
        private double numar1 = 0.0;
        private double total = 0.0;
        private double total2 = 0.0;
        private bool ClickPlus = false, ClickMinus = false, ClickInmultire = false, ClickImpartire = false;
        private bool WaitForClear = false;
        private double TotalMemory = 0.0;
        System.Windows.Forms.NotifyIcon ni = new System.Windows.Forms.NotifyIcon();
        public MainWindow()
        {
            InitializeComponent();
            DisplayBox.Text = "0";
            ni.Icon = new System.Drawing.Icon("Calculator.ico");
            ni.DoubleClick +=
                delegate (object sender, EventArgs args)
                {
                    this.Show();
                    this.WindowState = WindowState.Normal;
                    ni.Visible = false;
                };
            MemoryCall.IsEnabled = false;
            MemoryRecall.IsEnabled = false;
        }
        protected override void OnStateChanged(EventArgs e)
        {
            if (WindowState == System.Windows.WindowState.Minimized)
            {
                ni.Icon = new System.Drawing.Icon("Calculator.ico");
                ni.Visible = true;
                this.Hide();
            }
            base.OnStateChanged(e);
        }
        private void DigitClicked(object sender, RoutedEventArgs e)
        {
            string s = ((System.Windows.Controls.Button)sender).Content.ToString();
            if (DisplayBoxLength(DisplayBox.Text))
            {
                if (WaitForClear == true)
                {
                    ClearAll();
                    WaitForClear = false;
                }
                if (DisplayBox.Text.Equals("0"))
                {
                    DisplayBox.Clear();
                    DisplayBox.Text += s;
                }
                else
                {
                    DisplayBox.Text += s;
                }
                numar1 = Double.Parse(DisplayBox.Text);
                DisplayBox.Text = numar1.ToString();
            }        

        }
        //private void digitGroupping(string aux)
        //{
        //    if (DigitGrouppingCheck.IsChecked)
        //    {
        //        double value = Double.Parse(DisplayBox.Text);
        //        DisplayBox.Text = value.ToString("#,##.###########");
        //    }
        //    else
        //    {
        //        double value = Double.Parse(DisplayBox.Text);
        //        DisplayBox.Text = value.ToString("#,##.###########");
        //    }
        //}
        //clearOperations
        private void ClearAll()
        {
            numar1 = 0;
            total = 0;
            total2 = 0;
            DisplayBox.Clear();
            ClickPlus = false;
            ClickMinus = false;
            ClickInmultire = false;
            ClickImpartire = false;
        }
        private void ClearEntry_Click(object sender, RoutedEventArgs e)
        {
            DisplayBox.Clear();
            DisplayBox.Text = "0";

        }
        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            numar1 = 0;
            total = 0;
            total2 = 0;
            DisplayBox.Clear();
            ClickPlus = false;
            ClickMinus = false;
            ClickInmultire = false;
            ClickImpartire = false;
        }
        private void Punct_Clik(object sender, RoutedEventArgs e)
        {
            string s = ((System.Windows.Controls.Button)sender).Content.ToString();
            if (DisplayBox.Text.Contains(".") == false)
            {
                DisplayBox.Text += s;
            }
        }
        //Cut copy paste backspace
        private void Backspace_Click(object sender, RoutedEventArgs e)
        {
            DisplayBox.Text = DisplayBox.Text.Remove(DisplayBox.Text.Length - 1);
            if (DisplayBox.Text.Equals(""))
                DisplayBox.Text = "0";
        }
        private void MenuItem_Copy(object sender, RoutedEventArgs e)
        {
            System.Windows.Clipboard.SetText(DisplayBox.Text);
        }
        private void MenuItem_Cut(object sender, RoutedEventArgs e)
        {
            System.Windows.Clipboard.SetText(DisplayBox.Text);
            DisplayBox.Text = "0";
        }
        private void Menutem_Paste(object sender, RoutedEventArgs e)
        {
            string check = System.Windows.Clipboard.GetText();
            bool result = check.Any(x => !char.IsLetter(x));
            if (result == true)
                DisplayBox.Text = System.Windows.Clipboard.GetText();
        }
        //Radical, x^2, 1/x, +-
        private void SingleNumberOperations(object sender, RoutedEventArgs e)
        {
            string s = ((System.Windows.Controls.Button)sender).Content.ToString();
            double convertor;
            var button = sender as System.Windows.Controls.Button;
            if (Double.TryParse(DisplayBox.Text, out convertor))
            {
                switch (button.Name)
                {
                    case "Radical":
                        if (convertor < 0)
                        {
                            System.Windows.Forms.MessageBox.Show("Nu se poate scoate radical din numar negativ", "NaN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ClearAll();
                        }
                        else
                        {
                            convertor = Math.Sqrt(convertor);
                            DisplayBox.Text = convertor.ToString();
                        }
                        break;
                    case "XPatrat":
                        convertor *= convertor;
                        DisplayBox.Text = convertor.ToString();
                        break;
                    case "UnuPeX":
                        if (convertor == 0)
                        {
                            System.Windows.Forms.MessageBox.Show("Nu se poate imparti la 0", "NaN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            ClearAll();
                        }
                        else
                        {
                            convertor = 1 / convertor;
                            DisplayBox.Text = convertor.ToString();
                        }
                        break;
                    case "PlusMinus":
                        convertor = -convertor;
                        DisplayBox.Text = convertor.ToString();
                        numar1 = convertor;
                        break;
                    default:
                        throw new InvalidOperationException("Nu e bun");
                }
            }
        }
        //Operatii la click
        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            double value = Double.Parse(DisplayBox.Text);
            total = value;
            DisplayBox.Clear();
            ClickPlus = true;
            ClickMinus = false;
            ClickInmultire = false;
            ClickImpartire = false;
        }
        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            double value = Double.Parse(DisplayBox.Text);
            total = value;
            DisplayBox.Clear();
            ClickPlus = false;
            ClickMinus = true;
            ClickInmultire = false;
            ClickImpartire = false;
        }
        private void Inmultire_Click(object sender, RoutedEventArgs e)
        {
            double value = Double.Parse(DisplayBox.Text);
            total += value;
            DisplayBox.Clear();
            ClickPlus = false;
            ClickMinus = false;
            ClickInmultire = true;
            ClickImpartire = false;
        }
        private void Impartire_Click(object sender, RoutedEventArgs e)
        {
            double value = Double.Parse(DisplayBox.Text);
            total += value;
            DisplayBox.Clear();
            ClickPlus = false;
            ClickMinus = false;
            ClickInmultire = false;
            ClickImpartire = true;
        }
        private void Egal_Click(object sender, RoutedEventArgs e)
        {
            double value = Double.Parse(DisplayBox.Text);
            if (ClickPlus == true)
                total2 = total + value;
            else if (ClickMinus == true)
                total2 = total - value;
            else if (ClickInmultire == true)
                total2 = total * value;
            else if (ClickImpartire == true)
                if (value == 0)
                {
                    System.Windows.Forms.MessageBox.Show("Nu se poate imparti la 0", "NaN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ClearAll();
                }
                else
                    total2 = total / value;
            DisplayBox.Text = total2.ToString();
            WaitForClear = true;
            total = 0;
        }
        //Apasare butoane tastatura
        private double GetDigitPressed(System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.NumPad0:
                    return 0;
                case Key.NumPad1:
                    return 1;
                case Key.NumPad2:
                    return 2;
                case Key.NumPad3:
                    return 3;
                case Key.NumPad4:
                    return 4;
                case Key.NumPad5:
                    return 5;
                case Key.NumPad6:
                    return 6;
                case Key.NumPad7:
                    return 7;
                case Key.NumPad8:
                    return 8;
                case Key.NumPad9:
                    return 9;

                case Key.D0:
                    return 0;
                case Key.D1:
                    return 1;
                case Key.D2:
                    return 2;
                case Key.D3:
                    return 3;
                case Key.D4:
                    return 4;
                case Key.D5:
                    return 5;
                case Key.D6:
                    return 6;
                case Key.D7:
                    return 7;
                case Key.D8:
                    return 8;
                case Key.D9:
                    return 9;
            }
            return -1;
        }
        private string GetSymbolPressed(System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Divide:
                    return "/";
                case Key.Multiply:
                    return "*";
                case Key.OemPlus:
                    return "+";
                case Key.OemMinus:
                    return "-";
                case Key.Decimal:
                    return ".";
                case Key.Enter:
                    return "Enter";
                case Key.Escape:
                    return "Escape";
                case Key.Back:
                    return "Backspace";
                case Key.Add:
                    return "+";
                case Key.Subtract:
                    return "-";
            }
            return "Hold my beer dude";
        }

        private void DigitPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (GetSymbolPressed(e) != "Hold my beer dude")
            {
                SymbolPressed(sender, e);
                e.Handled = true;
                return;
            }

            if (WaitForClear == true)
            {
                ClearAll();
                WaitForClear = false;
            }
            double number = GetDigitPressed(e);
            if (DisplayBoxLength(DisplayBox.Text))
            {
                if (DisplayBox.Text.Equals("0"))
                {
                    DisplayBox.Clear();
                    if (number != -1)
                        DisplayBox.Text += GetDigitPressed(e).ToString();
                }
                else
                  if (number != -1)
                    DisplayBox.Text += GetDigitPressed(e).ToString();
            }
        }
        private void SymbolPressed(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (GetSymbolPressed(e) == "+")
            {
                ClickPlus = true;
                ClickMinus = false;
                ClickInmultire = false;
                ClickImpartire = false;
                if (Double.TryParse(DisplayBox.Text, out double value))
                {
                    total = value;
                    DisplayBox.Clear();
                }

            }
            else if (GetSymbolPressed(e) == "-")
            {
                ClickPlus = false;
                ClickMinus = true;
                ClickInmultire = false;
                ClickImpartire = false;
                if (Double.TryParse(DisplayBox.Text, out double value))
                {
                    total = value;
                    DisplayBox.Clear();
                }
            }
            else if (GetSymbolPressed(e) == "*")
            {
                ClickPlus = false;
                ClickMinus = false;
                ClickInmultire = true;
                ClickImpartire = false;
                if (Double.TryParse(DisplayBox.Text, out double value))
                {
                    total = value;
                    DisplayBox.Clear();
                }
            }
            else if (GetSymbolPressed(e) == "/")
            {
                ClickPlus = false;
                ClickMinus = false;
                ClickInmultire = false;
                ClickImpartire = true;
                if (Double.TryParse(DisplayBox.Text, out double value))
                {
                    total = value;
                    DisplayBox.Clear();
                }
            }
            else if (GetSymbolPressed(e) == "Enter")
            {
                double value = double.Parse(DisplayBox.Text);
                if (ClickPlus == true)
                    total2 = total + value;
                else if (ClickMinus == true)
                    total2 = total - value;
                else if (ClickInmultire == true)
                    total2 = total * value;
                else if (ClickImpartire == true)
                    if (value == 0)
                    {
                        System.Windows.Forms.MessageBox.Show("Nu se poate imparti la 0", "NaN", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        ClearAll();
                    }
                    else
                        total2 = total / value;
                DisplayBox.Text = total2.ToString();
                CopyDisplayBox.Clear();
                WaitForClear = true;
                total = 0;
            }
            else if (GetSymbolPressed(e) == ".")
            {
                if (DisplayBox.Text.Contains(".") == false)
                {
                    DisplayBox.Text += ".";
                }
            }
            else if (GetSymbolPressed(e) == "Backspace")
            {
                DisplayBox.Text = DisplayBox.Text.Remove(DisplayBox.Text.Length - 1);
                if (DisplayBox.Text.Equals(""))
                    DisplayBox.Text = "0";
            }
            else if (GetSymbolPressed(e) == "Escape")
            {
                ClearAll();
            }
        }
        //Memories
        private void MemoryCall_Click(object sender, RoutedEventArgs e)
        {
            TotalMemory = 0;
            MemoryCall.IsEnabled = false;
            MemoryRecall.IsEnabled = false;
            DisplayBox.Text = 0.ToString();
        }
        private void MemoryRecall_Click(object sender, RoutedEventArgs e)
        {
            DisplayBox.Text = TotalMemory.ToString();
        }
        private void MemoryPlus_Click(object sender, RoutedEventArgs e)
        {
            TotalMemory += Double.Parse(DisplayBox.Text);
            MemoryCall.IsEnabled = true;
            MemoryRecall.IsEnabled = true;
        }
        private void MemoryMinus_Click(object sender, RoutedEventArgs e)
        {
            TotalMemory -= Double.Parse(DisplayBox.Text);
            MemoryCall.IsEnabled = true;
            MemoryRecall.IsEnabled = true;
        }
        private void MemoryStore_Click(object sender, RoutedEventArgs e)
        {
            TotalMemory = Double.Parse(DisplayBox.Text);
        }
        //MenuItems
        private void About_Click(object sender, RoutedEventArgs e)
        {
            Page1 page = new Page1();
            page.Show();
        }
        private void Procent_Click(object sender, RoutedEventArgs e)
        {
            double valueProcent = total;
            double num1 = Double.Parse(DisplayBox.Text);
            double number = num1 / 100 * valueProcent;
            DisplayBox.Text = number.ToString();

        }
        //private void digitGroupping(double aux)
        //{
        //    CultureInfo customCulture = (System.Globalization.CultureInfo)
        //    System.Globalization.CultureInfo.CurrentCulture.Clone();
        //    customCulture.NumberFormat.NumberDecimalSeparator = ",";
        //    customCulture.NumberFormat.NumberGroupSeparator = ".";
        //    DisplayBox.Text += aux.ToString("#,##0.###", customCulture);
        //}
        private bool DisplayBoxLength(string text)
        {
            if (text.Length > 15)
                return false;
            return true;
        }
    }
}
